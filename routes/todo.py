"""
Defines the blueprint for the todos
"""
from flask import Blueprint
from flask_restful import Api
from resources import TodosResource

TODO_BLUEPRINT = Blueprint('todo', __name__)
Api(TODO_BLUEPRINT).add_resource(TodosResource, '/todos/<int:user_id>', '/todos/<int:user_id>/<int:todo_id>')
