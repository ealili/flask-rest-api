from . import db
from .abc import BaseModel, MetaBaseModel


class Todo(db.Model, BaseModel, metaclass=MetaBaseModel):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(50))
    complete = db.Column(db.Boolean)
    user_id = db.Column(db.Integer)
