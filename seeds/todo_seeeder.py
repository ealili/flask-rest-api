from models import Todo, User
from seeds.base_seeder import BaseSeeder


class TodoSeeder(BaseSeeder):

    # run() will be called by Flask-Seeder
    def run(self):
        john_id = User.query.filter_by(name='Robert Smith').first().id
        robert_id = User.query.filter_by(name='Mary Evans').first().id
        todos = [
            Todo(text='Finish Flask project', complete=False, user_id=john_id),
            Todo(text='Finish capstone project', complete=False, user_id=john_id),
            Todo(text='Walk the dog', complete=False, user_id=robert_id)
        ]

        # Create users
        for todo in todos:
            print(f'Adding todo: {todo.json}')
            self.db.session.add(todo)
