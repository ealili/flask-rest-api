# Flask REST API

This is a very simple project done using flask. It's a RESTful API where
users can be created and have their todos. The authentication part is 
not done yet but it will soon be implemented using jwt token based 
authentication.


## Table of Contents

1. [Getting Started](#getting-started)
1. [Commands](#commands)
1. [Resources](#resources)


## Getting Started

What you will need

* [**Python**](https://www.python.org/)
* [**pip** package installer](https://pypi.org/project/pip/)
* [**PostgreSQL**](https://www.postgresql.org/)

First, clone the project:

```bash
$ git clone https://gitlab.com/ealili/flask-rest-api.git
$ cd flask-rest-api
```

#### Setup Database URI

The database is in [PostgreSql](https://www.postgresql.org/).

Before running the application, firstly open the project using your favorite IDE.


In the root folder there is a file **config.py** where you will find the DB_URI configuration which
 in my case is **DB_URI = "postgresql://postgres:postgres@localhost:5432/flaskdb"**. 
 
Configure it to your own postgres database otherwise the application will not run.


#### Set up virtual environment and run the project

For windows:

```bash
$ python -m venv venv               # Create virtual environment
$ venv\Scripts\activate             # Activate virtual environment
$ pip install -r requirements.txt   # Install pip dependencies
$ python init_db.py                 # Create database tables
$ flask seed run                    # Run seeds to fill the database
$ flask run                         # Run the project     
```

The app run locally on port 5000.

If everything works, you should see the "Hello World!" [here](http://127.0.0.1:5000/api/api).


## Commands

Useful commands

| Command      | Description                                                                  |
| --------------------  | ---------------------------------------------------------------------------- |
| `python init_db`      | Initialize database schemas                |
| `python drop_db`      | Drop database schemas                      |
| `flask run`           | Run app                                    |
| `flask seed run`| Run seeders|



## Resources

| Resources on `/api`            | Method        | Description                                 | Status Code |
| ----------------------  | --------------| ------------------------------------------- | ----------- |
| `/users`                | GET           | Returns all users                               |200|
| `/user/`                | POST          | Creates a new user by id                        |201|
| `/user/<int:public_id>`   | GET           | Returns a user by id                            |200|
| `/user/<int:public_id>`   | DELETE        | Deletes a user by id                            |204|
| `/todos/<int:user_id>`  | POST          | Create a todo                                   |201|
| `/todos/<int:user_id>`  | GET           | Returns all todos by user id                    |200|
| `/todos/<int:user_id>/<int:todo_id>`| GET | Returns todo by user and todo id              |200|
| `/todos/<int:user_id>/<int:todo_id>` | DELETE | Deletes a todo by user and todo id         |204|

### Creating a user

`POST /api/user`

```json
{
    "name": "John Doe",
    "password": "johndoe123"
}
```

`Response`

```json
{
    "user": {
        "id": 1
        "public_id": "435459ce-3d37-4016-9381-74a71dc637f8",
        "name": "John Doe",
        "password": "sha256$dl9y0Ymu$ac13475bf3ec548521912a836303a1a93435ebbb7d0702d4c21bf3fdd4f1ca90",
        "admin": false
    }
}
```


### Creating a todo

`POST /api/user/1 `
```json
{
    "text": "Finish flask project"
}
```

`Response`

```json
{
    "todo": {
        "id": 1,
        "text": "Finish flask project",
        "complete": false,
        "user_id": 1
    }
}
```
