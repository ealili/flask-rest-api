"""
Define the REST verbs relative to the api
"""

from flask_restful import Resource
from repositories import ApiRepository


class ApiResource(Resource):
    """ Verbs relative to the api """

    @staticmethod
    def get():
        return ApiRepository.get()
