from seeds.todo_seeeder import TodoSeeder
from seeds.user_seeder import UserSeeder

from flask_seeder import Seeder


class AllSeeders(Seeder):
    # run() will be called by Flask-Seeder
    def run(self):
        # Add users first to have user id's
        user_seeder = UserSeeder(self.db)
        user_seeder.run()

        # Add todos
        todo_seeder = TodoSeeder(self.db)
        todo_seeder.run()
