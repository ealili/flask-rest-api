import uuid

from models import User
from seeds.base_seeder import BaseSeeder


class UserSeeder(BaseSeeder):

    # run() will be called by Flask-Seeder
    def run(self):
        users = [User(public_id=str(uuid.uuid4()), name='John Doe', password='johndoe123', admin=False),
                 User(public_id=str(uuid.uuid4()), name='Robert Smith', password='robert123', admin=False),
                 User(public_id=str(uuid.uuid4()), name='Mary Evans', password='maryevans123', admin=False)]

        # Create users
        for user in users:
            print(f'Adding user: {user.json}')
            self.db.session.add(user)
