""" Defines the User repository """
import uuid

from werkzeug.security import generate_password_hash

from models import User


class UserRepository:
    """ The repository for the user  """

    @staticmethod
    def get_all():
        return User.query.all()

    @staticmethod
    def get(public_id):
        return User.query.filter_by(public_id=public_id).first()

    @staticmethod
    def create(name, password):
        hashed_password = generate_password_hash(password, method='sha256')

        new_user = User(public_id=str(uuid.uuid4()), name=name, password=hashed_password,
                        admin=False)

        return new_user.save()

    @staticmethod
    def delete(public_id):
        user = UserRepository.get(public_id)
        user.delete()
