"""
Define the REST verbs relative to the api
"""
from flask import request
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound

from models import Todo
from repositories import TodoRepository


class TodosResource(Resource):
    """ Verbs relative to the api """

    @staticmethod
    def get(user_id):
        todos = [todo.json for todo in TodoRepository.get_all(user_id)]
        return {'todos': todos}

    @staticmethod
    def post(user_id):
        data = request.get_json()

        new_todo = TodoRepository.create(user_id=user_id, text=data['text'])

        return {'todo': new_todo.json}, 201

    @staticmethod
    def delete(user_id, todo_id):
        try:
            todo = Todo.query.filter_by(id=todo_id, user_id=user_id).first()
        except NoResultFound:
            return {'message': 'Todo found!'}, 404

        TodoRepository.delete(todo)

        return {'message': 'The todo has been deleted!'}, 204
