from flask import Flask, Blueprint
from flask_seeder import FlaskSeeder

import config
import routes
from models import db

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.init_app(app)
db.app = app

seeder = FlaskSeeder()
seeder.init_app(app, db)


@app.errorhandler(404)
def page_not_found(e):
    return {'message': str(e)}, 404


for blueprint in vars(routes).values():
    if isinstance(blueprint, Blueprint):
        app.register_blueprint(blueprint, url_prefix=config.APPLICATION_ROOT)

if __name__ == '__main__':
    app.run(port=config.PORT)
