import json
import unittest

from models import User
from models.abc import db
from app import app


class TestUser(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = app.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on `/user` should return an user """
        user = User(id=1, public_id="6a5e8f6c-20aa-4f1c-999a-f94c351ecd5b", name="John Doe", password="johndoe123",
                    admin=False)
        db.session.add(user)
        db.session.commit()
        response = self.client.get("/api/user/6a5e8f6c-20aa-4f1c-999a-f94c351ecd5b")

        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.data.decode("utf-8"))
        self.assertEqual(
            response_json,
            {"user": {"id": 1, "public_id": "6a5e8f6c-20aa-4f1c-999a-f94c351ecd5b", "name": "John Doe",
                      "password": "johndoe123", "admin": False}},
        )

    def test_create(self):
        """ The POST on `/user` should create an user """
        response = self.client.post(
            "/api/user",
            content_type="application/json",
            data=json.dumps({
                "name": "John Doe",
                "password": "johndoe123"
            }),
        )

        self.assertEqual(response.status_code, 201)


if __name__ == '__main__':
    unittest.main()
