"""
Define the REST verbs relative to the users
"""

from flask_restful import Resource

from repositories import UserRepository


class UsersResource(Resource):
    """ Verbs relative to the users """

    @staticmethod
    def get():
        users = [user.json for user in UserRepository.get_all()]
        return {'users': users}
