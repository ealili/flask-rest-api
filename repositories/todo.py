""" Defines the Todo repository """
from models import Todo


class TodoRepository:
    """ The repository for the todo """

    @staticmethod
    def get_all(user_id):
        return Todo.query.filter_by(user_id=user_id).all()

    @staticmethod
    def create(user_id, text):
        new_todo = Todo(text=text, complete=False, user_id=user_id)

        return new_todo.save()

    @staticmethod
    def delete(todo):
        todo.delete()
