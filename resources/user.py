"""
Define the REST verbs relative to the user
"""

from flask import request
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound

from repositories import UserRepository


class UserResource(Resource):
    """ Verbs relative to the user """

    @staticmethod
    def get(public_id):
        try:
            user = UserRepository.get(public_id)
        except NoResultFound:
            return {'message': 'User not found'}, 404

        return {'user': user.json}

    @staticmethod
    def post():
        data = request.get_json()

        new_user = UserRepository.create(data['name'], data['password'])

        return {'user': new_user.json}, 201

    @staticmethod
    def delete(public_id):
        try:
            UserRepository.get(public_id)
        except NoResultFound:
            return {'message': 'No user found!'}, 404

        UserRepository.delete(public_id)

        return {'message': 'The user has been deleted!'}, 204
